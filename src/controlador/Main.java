/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;


import java.io.IOException;
import modelo.Persona;
import vista.Vista;

/**
 *
 * @author Rafael Partida Ibáñez (1516Ceed72)
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
       Persona persona;
       Vista vista = new Vista();
       persona = vista.pedirDatos();
       vista.mostrarDatos(persona);
    
      
    }
    
}
