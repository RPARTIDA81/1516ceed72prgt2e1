/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rafael Partida Ibáñez (1516Ceed72)
 */
public class Persona {

    private String nombre;
    private int edad;

    /**
     * @return the nombre
     */
    public String getPersona() {
        return nombre;
    }

    /**
     * @param persona the nombre to set
     */
    public void setPersona(String persona) {
        this.nombre = persona;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    
}