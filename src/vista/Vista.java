/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import modelo.Persona;
import java.io.*;

/**
 *
 * @author Rafael Partida Ibáñez (1516Ceed72)
 */
public class Vista {
    
    //método para pedir datos por teclado
    public Persona pedirDatos() throws IOException{
        Persona persona = new Persona();
        
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        
        System.out.println("¿Cual es tu nombre?");
        String nombre = buffer.readLine();
        persona.setPersona(nombre);
        
        int numero;
        String linea;
        System.out.println("¿Que edad tienes?");
        linea = buffer.readLine();
        numero = Integer.parseInt(linea);
        persona.setEdad(numero);
        return persona;
        
    
    }
    public void mostrarDatos(Persona persona){
        System.out.println("La persona se llama "+persona.getPersona());
        System.out.println("La edad de "+persona.getPersona()+" es de "
                            +persona.getEdad()+" años");
    }
}
